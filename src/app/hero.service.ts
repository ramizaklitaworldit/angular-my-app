// import { Injectable } from '@angular/core';
// import { Hero } from './hero';
// import { HEROES } from './mock-heroeslist';

// import { MessageService } from './message.service';

// import { Observable, of } from 'rxjs';

// @Injectable({
//   providedIn: 'root'
// })
// export class HeroService {

//   constructor(private messageService: MessageService) { }


//   getHeroes(): Observable<Hero[]> {
//     // TODO: send the message _after_ fetching the heroes
//     this.messageService.add('HeroService: fetched heroes');
//     return of(HEROES);
//   }

//   getHero(id: number): Observable<Hero> {
//     // TODO: send the message _after_ fetching the hero
//     this.messageService.add(`HeroService: fetched hero id=${id}`);
//     return of(HEROES.find(hero => hero.id === id));
//   }

//   // getHeroes(): Observable<Hero[]> {
//   //   return of(HEROES);
//   // }

//   // getHeroes(): Hero[] {
//   //   return HEROES;
//   // }
// }


import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { Hero } from './hero';
import { HEROES } from './mock-heroeslist';
import { MessageService } from './message.service';


@Injectable({ providedIn: 'root' })
export class HeroService {
  private heroesUrl = 'api/heroes';  // URL to web api
  constructor(
    private http: HttpClient,
    private log(message: string) {
      this.messageService.add(`HeroService: ${message}`);
    }
    //private messageService: MessageService
    ) { }

  getHeroes(): Observable<Hero[]> {
    // TODO: send the message _after_ fetching the heroes
    //this.messageService.add('HeroService: fetched heroes');
    //return of(HEROES);
    return this.http.get<Hero[]>(this.heroesUrl)
  }

  getHero(id: number): Observable<Hero> {
    // TODO: send the message _after_ fetching the hero
    this.messageService.add(`HeroService: fetched hero id=${id}`);
    return of(HEROES.find(hero => hero.id === id));
  }
}